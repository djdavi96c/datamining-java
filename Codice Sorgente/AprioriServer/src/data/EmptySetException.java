package data;

/**
 * La classe EmptySetException modella l'eccezzione qualora
 * l'insieme di training fosse vuoto (Non contiene Transazioni/Esempi)
 *  @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone
 */
public class EmptySetException extends Exception
{
	
	/**
	 * Instantiates a new empty set exception.
	 */
	public EmptySetException(){};
	
	/**
	 * Instantiates a new empty set exception.
	 *
	 * @param msg the msg
	 */
	public EmptySetException(String msg)
	{
		super(msg);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Throwable#getMessage()
	 */
	public String getMessage()
	{
		return "EmptySetException";
	}
}
