package data;

import java.util.Iterator;


/**
 * La classe ContinuousAttribute  che estende la classe Attribute e modella un attributo continuo (numerico) rappresentandone il dominio [min,max]. implementi l'interfaccia Iterable di tipo Float  
 * @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone
 */
public class ContinuousAttribute extends Attribute implements Iterable<Float> {
		
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -3996133418273740519L;
	
	/** Il minimo */
	private float min;
	
	/** Il massimo */
	private float max;
	
	/**
	 * Instanzia un ContinuousAttribue
	 *
	 * @param name il nome dell'attributo
	 * @param index l'indice dell'attributo
	 * @param min il minimo dell'intervallo
	 * @param max il massimo dell'intervallo
	 */
	ContinuousAttribute(String name, int index, float min, float max)
	{
		super(name,index);
		this.min=min;
		this.max=max;
	}
	
	/**
	 * Ottiene il minimo dell'intervallo
	 *
	 * @return the min il minimo dell'intervallo
	 */
	public float getMin()
	{
		return min;
	}
	
	/**
	 * Otteiene il massimo dell'intervallo
	 *
	 * @return the max il massimo dell'intervallo
	 */
	public float getMax()
	{
		return max;
	}
	

	/**
	 * Iteratore per il ContinuousAttribute
	 * @return un iteratore per il ContinuosAttribute
	 */
	@Override
	public Iterator<Float> iterator() 
	{
		return new ContinuousAttributeIterator(getMin(), getMax(), 5);
	}

}
