package data;

import java.io.Serializable;


/**
 * La classe astratta Attribute che modella un generico attributo discreto o continuo. 
 * @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone
 * 
 */
public abstract class Attribute implements Serializable 
{	
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 2416400680372454744L;
	
	/** Il nome dell'Attributo */
	private String name;
	
	/** L'indice dell'Attributo */
	private int index;
	
	/**
	 * Instanzia un nuovo attributo
	 *
	 * @param name il nome dell'attributo
	 * @param index l'indice dell'attributo
	 */
	Attribute(String name, int index)
	{
		this.name = name;
		this.index = index;
	}
	
	/**
	 * Ottenere il nome dell'attributo
	 *
	 * @return il nome dell'attributo
	 */
	public String getName()
	{	
		return name;
	}
	
	/**
	 * Restituisce l'indice
	 *
	 * @return index l'indice dell'attributo
	 */
	public int getIndex()
	{
		return index;
	}
	/**
	 * Restituisce il nome dell'attributo
	 * @return restrituisce il nome dell'attributo sotto forma di stringa
	 */
	public String toString()
	{
		return name;
	}
}
