package data;

import java.util.List;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.LinkedList;
import database.*;
import database.TableData.TupleData;


/**
 *La classe Data si occupa di per modellare un insieme di transazioni 
 */
public class Data {
	
	/** The data. */
	private Object data[][];
	
	/** The number of examples. */
	private int numberOfExamples;
	
	/** The attribute set. */
	private List<Attribute> attributeSet=new LinkedList<Attribute>();
	
	/**
	 * Instanzia un nuovo oggetto data
	 *
	 * @param table  il nome della tabella
	 * @throws SQLException the SQL exception
	 */
	public Data(String table) throws SQLException
	{
		data= new Object[14][5];
		DbAccess db = new DbAccess();
		
		try
		{
			db.initConnection();
		} catch (DatabaseConnectionException e1)
		{
			e1.printStackTrace();
		}
		TableData dBSchema = new TableData();
		dBSchema.DbAccess(db);
		List<TupleData> transSet = new LinkedList<TupleData>();
		
		try
		{
			transSet = dBSchema.getTransazioni(table);
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
		
		data = new Object[transSet.size()][transSet.get(0).tuple.size()];
		for(int i=0; i<transSet.size(); i++)
		{			
			for(int j =0; j<transSet.get(i).tuple.size(); j++)
			{
				data[i][j] = transSet.get(i).tuple.get(j);
			}
		}
			
		TableSchema attributeTable = new TableSchema(db, table);
		List<Object> attributeList = new LinkedList<Object>();
		
		for(int i=0; i<attributeTable.getNumberOfAttributes(); i++)
		{
			attributeList.add(dBSchema.getDistinctColumnVaues(table, attributeTable.getColumn(i)));
		}
		
		for(int i=0; i<attributeTable.getNumberOfAttributes(); i++)
		{
			if(!attributeTable.getColumn(i).isNumber())
			{
				Object[] temp=(Object[])((List<Attribute>)attributeList.get(i)).toArray();
				String distinctValues[]= Arrays.copyOf(temp, temp.length, String[].class);
				attributeSet.add(new DiscreteAttribute(attributeTable.getColumn(i).getColumnName(), i, distinctValues));
			}
			else {attributeSet.add(new ContinuousAttribute(attributeTable.getColumn(i).getColumnName(), i, 0.0f, 30.3f));}
		}
		
		numberOfExamples=transSet.size();
	}
	
	/**
	 * Gets the number of examples. Ottiene il numero degli esempi
	 *
	 * @return il numero degli esempi
	 */
	public int getNumberOfExamples()
	{
		return numberOfExamples;
	}
	
	/**
	 * Gets the number of attributes. Ottiene il numero di attributi
	 *
	 * @return il numero degli attributi
	 */
	public int getNumberOfAttributes()
	{
		return attributeSet.size();
	}
	
	/**
	 * Gets the attribute value. Ottiene il valore dell'attributo specifico
	 *
	 * @param exampleIndex l'indice dell'esempio
	 * @param attributeIndex l'indice dell'attributo
	 * @return il valore dell'attributo
	 */
	public Object getAttributeValue(int exampleIndex, int attributeIndex)
	{
		return data[exampleIndex][attributeIndex];
	}
	
	/**
	 * Gets the attribute. Ottiene l'attributo con indice specifico.
	 *
	 * @param attributeIndex ,l 'indice dell'attributo
	 * @return l'attributo
	 */
	public Attribute getAttribute(int attributeIndex)
	{
		return attributeSet.get(attributeIndex);	
	}
	
	/**
	 * Restituisce una stringa con tutti i dati
	 * @return restituisce tutti i dati contenuti in data
	 */
	public String toString()
	{
		String temp = "";
		int i,j;
		
		 for(i=0; i<numberOfExamples;i++)
		 {
			 for(j=0;j<attributeSet.size();j++)
			 {
				 temp =temp + data[i][j] + "," ;
				 		
			 }
			 temp = temp + "\n";			 
		 }	
		 
		 return temp;
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws SQLException the SQL exception
	 */
	public static void main(String args[]) throws SQLException 
	{
	Data x = new Data("playtennis");
	System.out.println(x);
	}
	
}







