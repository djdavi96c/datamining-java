package data;

import java.util.Iterator;

/**
 * La classe ContinuousAttributeIterator che implementa l�interfaccia Iterator di float.  Tale classe realizza l�iteratore che itera sugli elementi della sequenza composta da numValues valori reali equidistanti tra di loro (cut points) compresi tra min e max ottenuti per mezzo di discretizzazione. La classe implementa i metodi della interfaccia generica Iterator tipizzata con   Float 
 * @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone
 */
public class ContinuousAttributeIterator implements Iterator<Float> 
{
	
	/** Il min. */
	private float min;
	
	/** Il max. */
	private float max;
	
	/**  posizione dell�iteratore nella collezione di cut point generati per [min, max[ tramite discretizzazione */
	private int j=0;
	
	/**  numero di intervalli di discretizzazione  */
	private int numValues;
	
	/**
	 * Instanzia un nuovo ContinuousAttributeIterator
	 *
	 * @param min il minimo
	 * @param max il massimo
	 * @param numValues the num values
	 */
	ContinuousAttributeIterator(float min,float max,int numValues)
	{
		this.min=min;
		this.max=max;
		this.numValues=numValues;
	}
	/**
	 * hasNext
	 * @return restituisce true se j � minore o uguale a numValues, false altrimenti 
	 */
	@Override
	public boolean hasNext()
	{
		return (j<=numValues);
	}
	
	/**
	 * next
	 * @return restituisce il cut point in posizione j-1 (min + (j-1)*(max-min)/numValues)
	 */
	@Override
	public Float next()
	{
		j++;
		return min+((max-min)/numValues)*(j-1);
	}
	
	/* (non-Javadoc)
	 * @see java.util.Iterator#remove()
	 */
	@Override
	public void remove(){} 
}
