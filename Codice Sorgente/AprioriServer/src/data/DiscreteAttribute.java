package data;

/**
 *  la classe DiscreteAttribute  che estende la classe Attribute e modella un   attributo discreto rappresentando l�insieme di valori distinti del relativo dominio.
 *  @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone
 */
public class DiscreteAttribute extends Attribute 
{
	
	/** Un array di stringhe, una per ciascun valore  discreto   */
	private String values[];
	
	/**
	 * Instanzia un nuovo DiscreteAttribute
	 *
	 * @param name il nome del DiscreteAttribute
	 * @param index l'indice del DiscreteAttribute
	 * @param values array di stringhe, una per ciascun valore discreto
	 */
	public DiscreteAttribute(String name, int index, String values[])
	{
		super(name,index);
		this.values=values; 
	}
	
	/**
	 * Gets the number of distinct values. Ottiene il numero dei distinti valori
	 *
	 * @return restituisce il numero dei distinti valori
	 */
	public int getNumberOfDistinctValues()
	{
		return values.length;
	}
	
	/**
	 * Gets the value. Ottiene il valore di un discrete attibute preciso
	 *
	 * @param index l'indice
	 * @return  Restituisce il valore in posizione i del membro  values 
	 */
	public String getValue(int index)
	{
		return values[index];
	}
}
