package mining;


/**
 * La Classe OneLevelPatternException modella l'eccezzione che occorre allor quando
 * si stenta di creare una regola da pattern di lunghezza 1
 * @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone  
 */
public class OneLevelPatternException extends Exception
{
	
	/**
	 * Instantiates a new one level pattern exception.
	 */
	public OneLevelPatternException(){};
	
	/**
	 * Gets the message. Restituisce il messaggio
	 *
	 * @param FP il frequent pattern
	 * @return il message
	 */
	public String getMessage(FrequentPattern FP)
	{
		String msg = FP + "\nLenght of"+ FP + "is 1";
		return msg;
	}

}
