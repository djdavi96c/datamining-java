package mining;

import java.util.LinkedList;

import data.Data;
import data.Attribute;


/**
 * La classe AssociationRuleMiner  che modella la scoperta di regole di associazione confidenti a partire da un pattern frequente P 
 *@author Davide Caldarulo,Davide Ciccarone,Alessio Gernone  
 */
public class AssociationRuleMiner 
 {
	
	/**
	 * Confident association rule discovery.
	 * Per ogni pattern di fp, si generano le regole di associazione (chiamando confidentAssociationRuleDiscovery)  e si calcola la confidenza. Le regole confidenti sono inserite in una lista 
	 * @param data the data
	 * @param fp the fp
	 * @param minConf the min conf
	 * @return the linked list
	 * @throws OneLevelPatternException the one level pattern exception
	 */
	public static LinkedList<AssociationRule> confidentAssociationRuleDiscovery(Data data,FrequentPattern fp,float minConf) throws OneLevelPatternException	
	{
		LinkedList<AssociationRule> tempList = new LinkedList<AssociationRule>();
		if(fp.getPatternLength()==1) throw new OneLevelPatternException();
		
		for(int i=1; i<fp.getPatternLength(); i++)
		{
			AssociationRule AR = new AssociationRule(fp.getSupport());
			AR = confidentAssociationRuleDiscovery(data, fp, minConf, i);
			if(AR.getConfidence() >= minConf)
			{
				tempList.add(AR);
			}
		}
		
		return tempList;
	}


	/**
	 * Confident association rule discovery.
	 *    Crea una regola di associazione estraendo come antecedente l�insieme degli item posizionati in fp prima dell�indice iCut e come conseguente l�insieme degli item posizionati in fp dopo dell�indice iCut (compresi iCut). Si calcola la confidenza della regola
	 * @param data insieme di transazioni di training
	 * @param fp  pattern di origine dal quale generare la regola 
	 * @param minConf minima confidenza
	 * @param iCut the i cut
	 * @return the association rule
	 */
	private static AssociationRule confidentAssociationRuleDiscovery(Data data,FrequentPattern fp,float minConf, int iCut)
	{
	AssociationRule AR=new AssociationRule(fp.getSupport());
	
	for(int j=0;j<iCut;j++)
	{
		AR.addAntecedentItem(fp.getItem(j));		
	}
	for(int j=iCut;j<fp.getPatternLength();j++)
	{
		AR.addConsequentItem(fp.getItem(j));
	}	
	
	AR.setConfidence(AssociationRuleMiner.computeConfidence(data,AR));
	
	return AR;
}

/**
 * Compute confidence.
 *  Si ottiene da AR il numero di transazioni che supportano il pattern, si calcola il numero di transazione che supportano
 * @param data l'insieme delle transizioni
 * @param AR regola di associazione da cui calcolare la confidenza
 * @return confidenza di AR (# transazioni in cui il pattern AR.antecedente unione AR.consguente � verificato / (# transazioni in cui l�antecedente della regola AR.antecente � verificato) 
 */
private static float  computeConfidence(Data data, AssociationRule AR)
{
	FrequentPattern tempFP = new FrequentPattern();
	for(int j = 0; j<AR.getAntecedentLenght(); j++)
	{
		tempFP.addItem(AR.getAntecedentItem(j));
	}
	for(int j = 0; j<AR.getConsequentLenght(); j++)
	{
		tempFP.addItem(AR.getConsequentItem(j));
	}

	float num=0;
	for(int i=0;i<data.getNumberOfExamples();i++)
	{
		boolean isSupporting=true;
		for(int j=0;j<tempFP.getPatternLength();j++)
		{
			Item item=tempFP.getItem(j);
			Attribute attribute=item.getAttribute();
			Object valueInExample=data.getAttributeValue(i, attribute.getIndex());
			if(!item.checkItemCondition(valueInExample))
			{
				isSupporting=false;
				break; //the ith example does not satisfy fp
			}			
		}
		if(isSupporting)
			num++;
	}
	
	float den=0;
	for(int i=0;i<data.getNumberOfExamples();i++)
	{
		boolean isSupporting=true;
		for(int j=0;j<AR.getAntecedentLenght();j++)
		{
			Item item=AR.getAntecedentItem(j);
			Attribute attribute=item.getAttribute();
			Object valueInExample=data.getAttributeValue(i, attribute.getIndex());
			if(!item.checkItemCondition(valueInExample))
			{
				isSupporting=false;
				break; //the ith example does not satisfy fp
			}
			
		}
		if(isSupporting)
			den++;
	}
	
	float conf;
	conf = num/den;
	
	return conf;
}

}