package mining;
import data.ContinuousAttribute;


/**
 * La classe ContinuousItem  che estende la classe astratta Item e modella la coppia Attributo continuo - Intervallo di valori  (Temperature in [10;30.31[) 
 * @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone  
 */
public class ContinuousItem extends Item
{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 4592230221445285802L;

	/**
	 * Instantiates un nuovo ContinuousItem
	 * chiama il costruttore della superclasse passandogli come argomenti attribute e value. 
	 * @param attribute Un continuosAttribte
	 * @param value l'intervallo
	 */
	ContinuousItem(ContinuousAttribute attribute, Interval value)
	{
		super(attribute, value);
	}

	/**
	 * checkItemCondition
	 * verifica che il parametro value rappresenti un numero reale incluso tra gli estremi dell�intervallo associato allo item in oggetto (richiamare il metodo checkValueInclusion(_) della classe Interval).
	 */
	@Override
	public boolean checkItemCondition(Object value) 
	{
		
		//Interval iter = new Interval((((ContinuousAttribute) super.getAttribute()).getMin()),(((ContinuousAttribute) super.getAttribute()).getMin()));
		return ((Interval)getValue()).checkValueInclusion((float)value);
	}
	
	/**
	 * avvalora la stringa che rappresenta lo stato dell�oggetto (per esempio, Temperatura in [10;20.4[) e ne restituisce il riferimento.
	 */
	public String toString()
	{
		return this.getAttribute() + " in " +super.value; 
	}
}

	