package mining;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;


/**
 * la classe FrequentPattern  che rappresenta un itemset (o pattern) frequente.
 * @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone  
 */
public class FrequentPattern implements Serializable
{

    /** The fp. */
    private List<Item> fp= new LinkedList<Item>(); 
	
	/** The support. */
	private float support;
	
	
	/**
	 * Instantiates a new frequent pattern.
	 */
	FrequentPattern()
	{
		fp=new LinkedList <Item>();
	}
	
	/**
	 * Adds the item.
	 *
	 * @param item the item
	 */
	public void addItem(Item item)
	{
		fp.add(item);
	}
	
	/**
	 * Gets the item.
	 *
	 * @param index the index
	 * @return the item
	 */
	public Item getItem(int index)
	{
		return fp.get(index);
	}
	
	/**
	 * Gets the support.
	 *
	 * @return the support
	 */
	public float getSupport()
	{
		return support;
	}
	
	/**
	 * Gets the pattern length.
	 *
	 * @return the pattern length
	 */
	public int getPatternLength()
	{
		return fp.size();
	}
	
	/**
	 * Sets the support.
	 *
	 * @param support the new support
	 */
	public void setSupport(float support)
	{
		this.support = support;
	}
	
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString()
	{
		String value="";
		for(int i=0;i<fp.size()-1;i++)
			value+="(" + fp.get(i) + ")" +" AND ";
		if(fp.size()>0){
			value+="(" + fp.get(fp.size()-1) + ")";
			value+="[Support= "+support+"]";
		}
		
		return value;
	}


}
