package mining;


/**
 * La Classe NoPatternException modella il caso in cui nessuna regola confidente
 * � generata da un pattern frequente.
 * @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone  
 */
public class NoPatternException extends Exception
{
	
	/**
	 * Instanzia un no pattern exception.
	 */
	NoPatternException(){};
	
	/**
	 * Gets the message.Ottiene il messaggio
	 *
	 * @param FP il frequent pattern
	 * @return il messaggio
	 */
	public String getMessage(FrequentPattern FP)
	{
		return "No Associacion Rule from: "+ FP +" discovered ";
	}
}
