package mining;

import java.util.HashMap;//importo tutto
import java.util.Iterator;
import java.util.TreeSet;
import java.util.*;
import java.io.*;

/**
 * La classe AssociationRuleArchive modella un contenitore di pattern - regole di associazione.
 * @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone  
 */
public class AssociationRuleArchive implements Serializable
{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 6281688667286256563L;
	
	/**  � definito come contenitore Map, per le coppie di Pattern Frequente, {Insieme di Regole  di Associazione estratte  dal pattern chiave e ordinate in maniera crescente rispetto alla confidenza} . */
	private HashMap<FrequentPattern, TreeSet<AssociationRule>> archive;
	

	/**
	 * Instanzia una nuova association rule archive.
	 */
	public AssociationRuleArchive()
	{
		archive = new HashMap<FrequentPattern, TreeSet<AssociationRule>>();
	}

	/**
	 * Put.
	 *	aggiunge fp ad archive, se fp non vi � gi� contenuto come chiave.
	 * @param fp pattern (chiave) da aggiungere ad archive 
	 */
	public void put(FrequentPattern fp) 
	{

		if (archive.isEmpty())
		{
			archive = new HashMap<FrequentPattern, TreeSet<AssociationRule>>();
			archive.put(fp, new TreeSet<AssociationRule>());
		}

		else if (!archive.containsValue(fp))
		{

			archive.put(fp, new TreeSet<AssociationRule>());

		}

	}

	/**
	 * Put.
	 *  aggiunge e fp ad archive, se fp non vi � gi� contenuto come chiave. Altrimenti, inserisce  rule al TreeSet delle regole indicizzato da fp in archive. 
	 * @param fp pattern (chiave)
	 * @param rule regola di associazione estratta dal pattern 
	 *
	 */
	public void put(FrequentPattern fp, AssociationRule rule) 
	{
		TreeSet<AssociationRule> temp = new TreeSet<AssociationRule>();
		temp.add(rule);
		if (archive.isEmpty()) 
		{
			archive = new HashMap<FrequentPattern, TreeSet<AssociationRule>>();
			archive.put(fp, temp);
		}
		if (!archive.containsKey(fp))
			archive.put(fp, temp);
		else
		{
			TreeSet<AssociationRule> tempTS = new TreeSet<AssociationRule>();
			try
			{
				tempTS = getRules(fp);
				tempTS.add(rule);
				archive.put(fp, tempTS);
			} catch (NoPatternException e)
			{
				System.out.println(e.getMessage(fp));
			}
		}
	}

	/**
	 * Gets the rules.
	 * Se fp compare come chiave in archive restituisce il set di regole corrispondente, altrimenti solleva e propaga una eccezione NoPatternException. 
	 * @param fp  pattern (chiave) per il quale ricercare il set delle regole
	 * @return the rules
	 * @throws NoPatternException the no pattern exception
	 */
	public TreeSet<AssociationRule> getRules(FrequentPattern fp) throws NoPatternException 
	{

		TreeSet<AssociationRule> temp = new TreeSet<AssociationRule>();
		if (archive.containsKey(fp)) 
		{
			temp = archive.get(fp);
		} else 
		{
			throw new NoPatternException();
		}
		return temp;
	}

	/**
	 * Itera sulle chiavi di archive e concatena nella stringa da restituire ciascun pattern (chiave) e il corrispondente set di regole di associazione
	 */
	public String toString()
	{

		String temp = "";
		Set<FrequentPattern> tempSet = archive.keySet();

		Iterator<FrequentPattern> itS = tempSet.iterator();
		int i = 1;
		while (itS.hasNext())
		{
			FrequentPattern FP = itS.next();
			TreeSet<AssociationRule> tempTS = archive.get(FP);
			Iterator<AssociationRule> itTS = tempTS.iterator();
			temp += i + " " + FP + "\n";
			int j = 1;
			while (itTS.hasNext())
			{
				AssociationRule AR = itTS.next();
				temp +="  - " + AR + "\n";
				j++;
			}
			i++;
		}
		return temp;
	}

	/**
	 * Salva.
	 *
	 * @param nomeFile the nome file
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public void salva(String nomeFile) throws FileNotFoundException, IOException 
	{
		FileOutputStream out = new FileOutputStream(nomeFile);
		ObjectOutputStream s = new ObjectOutputStream(out);
		
		s.writeObject(this);// scrivo l'archivio
		s.flush();
		s.close();
		out.close();
		
		System.out.println("File serializzato con successo!");
	}

	/**
	 * Carica.
	 *
	 * @param nomeFile the nome file
	 * @return the association rule archive
	 * @throws FileNotFoundException the file not found exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws ClassNotFoundException the class not found exception
	 */
	public static AssociationRuleArchive carica(String nomeFile) throws FileNotFoundException, IOException, ClassNotFoundException
	{

		AssociationRuleArchive temp = new AssociationRuleArchive();
		FileInputStream in = new FileInputStream(nomeFile);
		ObjectInputStream s = new ObjectInputStream(in);
			
		temp=(AssociationRuleArchive) s.readObject();
		s.close();
		in.close();
		//System.out.println("File letto con successo!\n"+temp.archive);

		return temp;
	}

}