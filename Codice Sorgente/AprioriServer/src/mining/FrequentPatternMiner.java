package mining;

import java.util.Iterator;
import java.util.LinkedList;

import data.*;
import utility.Queue;
import utility.EmptyQueueException;


/**
 * La classe FrequentPatternMiner  che include i metodi per la scoperta di pattern frequenti con Algoritmo APRIORI
 * @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone  
 */
public class FrequentPatternMiner
{

	/**
	 * Frequent pattern discovery.
	 *  Genera tutti i pattern k=1 frequenti e per ognuno di  questi genera quelli con k maggiore di 1 richiamando expandFrequentPatterns () 
	 *
	 * @param data l'insieme delle transizioni di training
	 * @param minSup il minimo supporto
	 * @return lista linkata contenente i pattern frequenti scoperti in data 
	 * @throws EmptySetException the empty set exception
	 */
	public static LinkedList<FrequentPattern> frequentPatternDiscovery(Data data, float minSup) throws EmptySetException
	{
		Queue<FrequentPattern> fpQueue = new Queue<FrequentPattern>();
		LinkedList<FrequentPattern> outputFP = new LinkedList<FrequentPattern>();

		if (data.getNumberOfExamples() == 0) throw new EmptySetException();
		
		for  (int i = 0; i < data.getNumberOfAttributes(); i++)
		{
			Attribute currentAttribute = data.getAttribute(i);
			if (currentAttribute instanceof DiscreteAttribute)
			{
				for (int j = 0; j < ((DiscreteAttribute) currentAttribute).getNumberOfDistinctValues(); j++)
				{
					DiscreteItem item = new DiscreteItem((DiscreteAttribute) currentAttribute, ((DiscreteAttribute) currentAttribute).getValue(j));
					FrequentPattern fp = new FrequentPattern();
					fp.addItem(item);
					fp.setSupport(FrequentPatternMiner.computeSupport(data, fp));
					if (fp.getSupport() >= minSup)
					{ 
						fpQueue.enqueue(fp);
						outputFP.add(fp);
					}
				}
			} 
			else
			{
				Iterator<Float> it = ((ContinuousAttribute) currentAttribute).iterator();
				if (it.hasNext())
				{
					float inf = it.next();
					while (it.hasNext())
					{
						float sup = it.next();
						ContinuousItem item;
						if (it.hasNext()) item = new ContinuousItem((ContinuousAttribute) currentAttribute, new Interval(inf, sup));
						else item = new ContinuousItem((ContinuousAttribute) currentAttribute, new Interval(inf, sup + 0.01f * (sup - inf)));
						inf = sup;
						FrequentPattern fp = new FrequentPattern();
						fp.addItem(item);
						fp.setSupport(FrequentPatternMiner.computeSupport(data, fp));
						if (fp.getSupport() >= minSup)
						{ 
							fpQueue.enqueue(fp);
							outputFP.add(fp);
						}
					}
				}
			}
		}

		try
		{
			outputFP = expandFrequentPatterns(data, minSup, fpQueue, outputFP);
		} catch (EmptyQueueException e)
		{
			System.out.println(e.getMessage());
		}

		return outputFP;
	}

	/**
	 * Expand frequent patterns.
	 *
	 * @param data l'insieme delle transazioni
	 * @param minSup il minimo supporto
	 * @param fpQueue coda contenente i pattern da valutare
	 * @param outputFP lista dei pattern frequenti gi� estratti
	 * @return  lista linkata popolata con pattern frequenti a k>1 
	 * @throws EmptyQueueException the empty queue exception
	 */
	private static LinkedList<FrequentPattern> expandFrequentPatterns(Data data, float minSup,
			Queue<FrequentPattern> fpQueue, LinkedList<FrequentPattern> outputFP) throws EmptyQueueException
	{
		FrequentPattern fp = new FrequentPattern();
		float tempSupp = 0;
		if (fpQueue.isEmpty()) throw new EmptyQueueException();
		while (!fpQueue.isEmpty())
		{
			fp = (FrequentPattern) fpQueue.first();
			FrequentPattern temp = new FrequentPattern();
			temp = FrequentPatternMiner.ItemsToAdd(fp, data);
			for (int i = 0; i < temp.getPatternLength(); i++)
			{
				FrequentPattern fp2 = new FrequentPattern();
				fp2 = (FrequentPattern) fpQueue.first();
				FrequentPattern fp3 = new FrequentPattern();
				for (int j = 0; j < fp2.getPatternLength(); j++)
				{
					fp3.addItem(fp2.getItem(j));

				}
				fp3 = refineFrequentPattern(fp3, temp.getItem(i));
				tempSupp = FrequentPatternMiner.computeSupport(data, fp3);
				fp3.setSupport(tempSupp);
				if (tempSupp >= minSup)
				{
					fpQueue.enqueue(fp3);
					outputFP.add(fp3);
				}
			}
			fpQueue.dequeue();
		}
		return outputFP;
	}

	/**
	 * Compute support.
	 * Per ognuna delle transazioni memorizzate in dat, verifica se ognuno degli Item costituenti il pattern FP occorre
	 * @param data l�insieme delle transazioni di training
	 * @param FP e il pattern (FP) di cui calcolare il supporto 
	 * @return restituisce il supporto
	 */
	private static float computeSupport(Data data, FrequentPattern FP)
	{
		int suppCount = 0;
		for (int i = 0; i < data.getNumberOfExamples(); i++)
		{
			boolean isSupporting = true;
			for (int j = 0; j < FP.getPatternLength(); j++)
			{
				Item item = FP.getItem(j);
				Attribute attribute = item.getAttribute();
				Object valueInExample = data.getAttributeValue(i, attribute.getIndex());
				if (!item.checkItemCondition(valueInExample))
				{
					isSupporting = false;
					break; // the ith example does not satisfy fp
				}

			}
			if (isSupporting)
				suppCount++;
		}
		return ((float) suppCount) / (data.getNumberOfExamples());
	}

	/**
	 * Refine frequent pattern.
	 * Crea un nuovo pattern a cui aggiunge tutti gli item di FP e il parametro item 
	 * @param FP pattern da raffinare
	 * @param item l'item da aggiungere ad FP
	 * @return il frequent pattern con l'item 
	 */
	private static FrequentPattern refineFrequentPattern(FrequentPattern FP, Item item)
	{
		FrequentPattern temp = new FrequentPattern();
		for (int i = 0; i < FP.getPatternLength(); i++)
		{
			temp.addItem(FP.getItem(i));
		}
		temp.addItem(item);
		return temp;
	}

	/**
	 * Items to add.
	 *
	 * @param fp the fp
	 * @param data the data
	 * @return the frequent pattern
	 */
	private static FrequentPattern ItemsToAdd(FrequentPattern fp, Data data)
	{
		FrequentPattern temp = new FrequentPattern();
		boolean flag;
		for (int i = 0; i < data.getNumberOfAttributes(); i++)
		{
			flag = true;
			Attribute currentAttribute = data.getAttribute(i);
			if (currentAttribute instanceof DiscreteAttribute)
			{
				for (int j = 0; j < ((DiscreteAttribute) currentAttribute).getNumberOfDistinctValues(); j++)
				{
					DiscreteItem item = new DiscreteItem((DiscreteAttribute) currentAttribute,
							((DiscreteAttribute) currentAttribute).getValue(j));
					for (int k = 0; k < fp.getPatternLength(); k++)
					{
						if (fp.getItem(k).getAttribute().equals(currentAttribute))
							flag = false;
					}
					if (flag) temp.addItem(item);
				}
			} 
			else
			{
				Iterator<Float> it = ((ContinuousAttribute) currentAttribute).iterator();
				if (it.hasNext())
				{
					float inf = it.next();
					while (it.hasNext())
					{
						float sup = it.next();
						ContinuousItem item;
						if (it.hasNext()) item = new ContinuousItem((ContinuousAttribute) currentAttribute, new Interval(inf, sup));
						else item = new ContinuousItem((ContinuousAttribute) currentAttribute, new Interval(inf, sup + 0.01f * (sup - inf)));
						inf = sup;
						for (int k = 0; k < fp.getPatternLength(); k++)
						{
							if (fp.getItem(k).getAttribute().equals(currentAttribute))
								flag = false;
						}
						if (flag) temp.addItem(item);
					}
				}	
			}

		}
		return temp;
	}

}
