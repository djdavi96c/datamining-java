package mining;

import java.io.Serializable;

/**
 *  La classe AssociationRule  che modella una regola di associazione confidente 
 *   @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone  
 */
public class AssociationRule implements Comparable<AssociationRule> , Serializable
{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 224214363153278411L;
	
	/** Array di item che modella l'antecedente della regola di associazione inizialmente vuoto  */
	private Item antecedent[] = new Item[0];
	
	/** Array di item che modella il conseguente della regola di associazione inizialmente vuoto  */
	private Item consequent[] = new Item[0];
	
	/** il supporto */
	private float support;
	
	/** la confidenza */
	private float confidence;
	
	/**
	 * Instanzia una nuova AssociationRule
	 *
	 * @param support il supporto
	 */
	AssociationRule(float support)
	{
		this.support = support;
	}
	
	/**
	 * Gets the support.
	 *
	 * @return the support restituisce il supporto 
	 */
	public float getSupport()
	{
		return support;
	}
	
	/**
	 * Gets the confidence.
	 *
	 * @return the confidence la confidenza
	 */
	public float getConfidence()
	{
		return confidence;
	}
	
	/**
	 * Gets the antecedent lenght. Ottiene la lunghezza dell'array antecedent
	 *
	 * @return restituisce la lunghezza dell'antecendete
	 */
	public int getAntecedentLenght()
	{
		return antecedent.length;
	}
	
	/**
	 * Gets the consequent lenght. Ottiene la lunghezza dell'array consequent
	 *
	 * @return restituisce la lunghezza del conseguente
	 */
	public int getConsequentLenght()
	{
		return consequent.length;
	}
	
	/**
	 * Adds the antecedent item. Aggiunge un item all'array antecedent
	 *
	 * @param item l'Item da aggiungere
	 */
	public void addAntecedentItem(Item item)
	{
		int length =antecedent.length;
		
		Item temp []=new Item[length+1];
		System.arraycopy(antecedent, 0, temp, 0, length);
		temp [length]=item;
		antecedent=temp;
	}
	
	/**
	 * Adds the consequent item. Aggiunge un item all'array consequent
	 *
	 * @param item L'item da aggiungere
	 */
	public void addConsequentItem(Item item)
	{
		int length =consequent.length;
		
		Item temp []=new Item[length+1];
		System.arraycopy(consequent, 0, temp, 0, length);
		temp [length]=item;
		consequent=temp;
	}
	
	/**
	 * Gets the antecedent item. Ottiene un item contenuto in antecedent
	 *
	 * @param index l'indice
	 * @return restituisce l'Item contenuto nella posizione index dell'array antecedent
	 */
	public Item getAntecedentItem(int index)
	{
		return antecedent[index];
	}
	
	/**
	 * Gets the consequent item. Ottiene un item contenuto in consequent
	 *
	 * @param index the index
	 * @return restituisce l'item contenuto in posizione index nell'array consequent
	 */
	public Item getConsequentItem(int index)
	{
		return consequent[index];
	}
	
	/**
	 * Sets the confidence. Setta la nuova confidenza
	 *
	 * @param confidence la nuova confidenza
	 */
	public void setConfidence(float confidence)
	{
		this.confidence = confidence;
	}
	
	/**
	 * toString restituisce l'association rule con antecedente e conseguente e la rispettiva confidenza
	 * @return value la stringa
	 */
	public String toString()
	{
		String value="";
		for(int i=0;i<antecedent.length-1;i++)
			value+="(" + antecedent[i] + ")" +" AND ";
		if(antecedent.length>0){
			value+="(" + antecedent[antecedent.length-1] + ")";
			
		}
		value+="==>";
		for(int i=0;i<consequent.length-1;i++)
			value+="(" + consequent[i] + ")" +" AND ";
		if(consequent.length>0){
			value+="(" + consequent[consequent.length-1] + ")";
		}
		value+="[Confidence= "+confidence+"]";
		
		return value;
	}
	
	/**
	 * compareTo compara la confidenza di un'associaton rule
	 * con la corrente e determina quale sia maggione e quale sia minore
	 * @param AR associationRule
	 * @return restituisce se la corrente confidenza � maggiore o minore a quella passata in input
	 */
	@Override
	public int compareTo(AssociationRule AR)
	{
		if(this.getConfidence() < AR.getConfidence()) return -1;
		else return 1;
		
	}
	
	
	
}
