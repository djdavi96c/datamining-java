package mining;

import java.io.Serializable;


/**
 * La classe Interval che modella un intervallo reale [inf ,sup[ 
 * @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone  
 */
public class Interval implements Serializable
{
	
	/** estremo inferiore */
	private float inf;
	
	/** estremo superiore */
	private float sup;
	
	/**
	 * Instanzia un nuovo intervallo
	 *
	 * @param inf the inf
	 * @param sup the sup
	 */
	Interval(float inf, float sup)
	{
		this.inf = inf;
		this.sup = sup;
	}
	
	/**
	 * Sets the inf.
	 *
	 * @param inf il nuovo inf
	 */
	public void setInf(float inf)
	{
		this.inf = inf;
	}
	
	/**
	 * Sets the sup.
	 *
	 * @param sup il nuovo sup
	 */
	public void setSup(float sup)
	{
		this.sup = sup;
	}
	
	/**
	 * Gets the inf.
	 *
	 * @return the inf
	 */
	public float getInf()
	{
		return inf;
	}
	
	/**
	 * Gets the sup.
	 *
	 * @return the sup
	 */
	public float getSup()
	{
		return sup;
	}
	
	/**
	 * Check value inclusion.
	 *
	 * @param value the value
	 * @return true, if successful
	 */
	public boolean checkValueInclusion(float value)
	{
		return Float.compare(inf, value)<=0 && Float.compare(value, sup)<0;
	}
	
	/**
	 * @return restituisce l'inf e il sup sottoforma di stringa
	 */
	public String toString()
	{
		return "["+getInf()+" , "+getSup()+"[";
	}

}
