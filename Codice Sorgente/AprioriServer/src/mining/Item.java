package mining;

import java.io.Serializable;

import data.Attribute;


/**
 * La classe astratta Item che modella un generico item (coppia attributo-valore, per esempio Outlook=�Sunny�) 
 * @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone  
 */
abstract class Item implements Serializable
{

	/** Attributo coinvolto nell'item */
	private Attribute attributo;
	
	/** valore assegnato all'attributo*/
	protected Object value;
	
	/**
	 * Instanzia un nuovo Item
	 *
	 * @param attributo l'attributo
	 * @param value il valore da assegnare all'attributo
	 */
	Item(Attribute attributo, Object value)
	{
		this.attributo = attributo;
		this.value = value;
	}
	
	/**
	 * Gets the attribute.
	 *
	 * @return restituisce l'attributo
	 */
	public Attribute getAttribute()
	{
		return attributo;
	}
	
	/**
	 * Gets the value.
	 *
	 * @return restituisce il valore legato all'attributo
	 */
	public Object getValue()
	{
		return value;
	}
	
	/**
	 * Check item condition.
	 *
	 * @param value the value
	 * @return true, if successful
	 */
	public abstract boolean checkItemCondition(Object value);
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	/**
	 * restituisce una stringa nella forma attribute=value 
	 */
	public String toString()
	{
		String temp = attributo.getName() + "=" + value.toString();
		
		return temp;
	}
}
