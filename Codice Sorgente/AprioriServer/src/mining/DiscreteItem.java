package mining;



import data.DiscreteAttribute;

/**
 *  la classe concreta DiscreteItem  che estende la classe Item e rappresenta la coppia Attributo discreto - valore discreto (Outlook=�Sunny�) 
 *  @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone
 */
class DiscreteItem extends Item 
{
	
	/**
	 * Instanzia un nuovo DiscreteItem
	 *
	 * @param attributo l'attributo del DiscreteItem
	 * @param value il valore assegnato all'attributo
	 */
	DiscreteItem(DiscreteAttribute attributo, String value)
	{
		super(attributo, value);
	}
	

	/**
	 * checkItemCondition  verifica che il membro value sia uguale (nello stato) all�argomento passato come parametro della funzione 
	 * @param value il valore da controllare
	 * @return flag booleano che indica se � uguale o meno al membro value
	 */
	public boolean checkItemCondition(Object value)
	{
		boolean flag = false;
		
		if (this.getValue().equals(value))
			flag = true;
		
		return flag;
	}
}
