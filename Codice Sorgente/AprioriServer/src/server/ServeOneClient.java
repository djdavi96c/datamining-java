package server;

import java.net.Socket;
import java.net.SocketException;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.LinkedList;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import mining.AssociationRule;
import mining.AssociationRuleMiner;
import mining.AssociationRuleArchive;
import mining.FrequentPattern;
import mining.FrequentPatternMiner;
import mining.OneLevelPatternException;

import data.Data;
import data.EmptySetException;



/**
 *  la classe ServerOneClient estende la classe Thread 
 *  che modella la comunicazione con un unico client. 
 *  @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone
 */
class ServeOneClient extends Thread {
	
	/** Il socket. */
	private Socket socket;
	
	/** L' in. */
	private ObjectInputStream in; // stream con richieste del client
	
	/** L' out. */
	private ObjectOutputStream out;
	
	/** L' archivio. */
	private AssociationRuleArchive archive;

	/**
	 * Instanzia un nuovo serve one client.
	 *
	 * @param s il socket
	 * @throws  Segnala che IOException � stata sollevata.
	 */
	ServeOneClient(Socket s) throws IOException {
		socket = s;
		out = new ObjectOutputStream(socket.getOutputStream());
		in = new ObjectInputStream(socket.getInputStream());
		start();

	}

	
	
	/* (non-Javadoc)
	 * @see java.lang.Thread#run()
	 */
	public void run() {
		System.out.println("Nuovo client connesso");

		try {
			while (true) {
				int command = 0;
				command = ((Integer) (in.readObject())).intValue();
				switch (command) {
				case 1: // Learning a new archive from DB
					try {
						archive = new AssociationRuleArchive();

						String tableName = (String) in.readObject();
						Data trainingSet = new Data(tableName);
						Float minSup = (Float) in.readObject();
						Float minConf = (Float) in.readObject();
						LinkedList<FrequentPattern> outputFP = FrequentPatternMiner
								.frequentPatternDiscovery(trainingSet, minSup);
						Iterator<FrequentPattern> it = outputFP.iterator();
						while (it.hasNext()) {
							FrequentPattern FP = it.next();
							archive.put(FP);
							LinkedList<AssociationRule> outputAR = null;
							try {
								outputAR = AssociationRuleMiner.confidentAssociationRuleDiscovery(trainingSet, FP,
										minConf);
								Iterator<AssociationRule> itRule = outputAR.iterator();
								while (itRule.hasNext()) {
									archive.put(FP, itRule.next());
								}
							} catch (OneLevelPatternException e) {
								e.getMessage();
							}
						}

					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (EmptySetException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}

					finally {
						out.writeObject(archive.toString());
						break;
					}

				case 2: // SAVING ON File
					try {
						archive.salva("backup.dat");
					} catch (NullPointerException e) {
						e.getMessage();
					}
					break;

				case 3: // STORING PATTERNS and RULES stored from a FILE
					AssociationRuleArchive temp = new AssociationRuleArchive();
					temp = archive.carica("backup.dat");
					out.writeObject(temp.toString());
					break;

				default:
					System.out.println("COMANDO INESISTENTE");
					try {
						out.writeObject("COMANDO INESISTENTE");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}// END SWITCH
			}
		} catch (

		SocketException e) {
			return;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		finally {
			try {
				socket.close();
			} catch (IOException e) {
				System.out.println("Socket non chiuso!");
			}
		}

	}

}