package server;

import java.net.*;
import java.io.*;

/**
 * la classe MultiServer che modella un server in
 * grado di accettare la richiesta trasmesse da un generico Client e 
 * istanzia un oggetto della classe ServerOneClient che si occupera di servire le
 * richieste del client in un thred dedicato. Il Server sar� registrato su 
 * una porta predefinita (al di fuori del range 1-1024), per esempio 8080.   
 * @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone
 */
public class MultiServer
{
	
	/** La porta costante */
	public static final int PORT = 8080;
	
	/**
	 * Instanzia multi server.
	 *
	 * @throws Segnala che IOException � stata sollevata.
	 */
	MultiServer() throws IOException
	{
		run();
	}
	
	/**
	 * Run.
	 *
	 * @throws Segnala che IOException � stata sollevata.
	 */
	private void run() throws IOException
	{
		ServerSocket s = new ServerSocket(PORT);
		
		try
		{
			while(true)
			{
				Socket socket = s.accept();
				try
				{
					new ServeOneClient(socket);
				}catch(IOException e){socket.close();};
			}
		}finally{s.close();}
	}
	


	/**
	 * Il metodo principale
	 *
	 * @param args gli argomenti
	 * @throws IOException � stata sollevata.
	 */
	public static void main(String[] args) throws IOException
	{
		System.out.println("inizio");
		MultiServer ms = new MultiServer();
	}
	
}	