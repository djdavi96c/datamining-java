package utility;


/**
 *  la classe Queue che modella una struttura coda che � poi  usata come contenitore a modalit� FIFO per i pattern frequenti scoperti a livello k da usare per generare i pattern candidati a livello k+1 
 *@author Davide Caldarulo,Davide Ciccarone,Alessio Gernone  
 * @param <T> un tipo generico
 */
public class Queue <T>
{

		/** The begin. */
		private Record begin = null;

		/** The end. */
		private Record end = null;
		
		/**
		 * The Class Record.
		 */
		private class Record 
		{

	 		/** The elem. */
		 	public T elem;

	 		/** The next. */
		 	public Record next;

			/**
			 * Instantiates a new record.
			 *
			 * @param e the e
			 */
			public Record(T e) 
			{
				this.elem = e; 
				this.next = null;
			}
		}
		

		/**
		 * Checks if is empty.
		 *
		 * @return true, if is empty
		 */
		public boolean isEmpty()
		{
			return this.begin == null;
		}

		/**
		 * Enqueue.
		 *
		 * @param e the e
		 */
		public void enqueue(T e) 
		{
			if (this.isEmpty())
				this.begin = this.end = new Record(e);
			else 
			{
				this.end.next = new Record(e);
				this.end = this.end.next;
			}
		}


		/**
		 * First.
		 *
		 * @return the t
		 */
		public T first()
		{
			return this.begin.elem;
		}

		/**
		 * Dequeue.
		 */
		public void dequeue()
		{
			if(this.begin==this.end)
			{
				if(this.begin==null)
				System.out.println("The queue is empty!");
				else
					this.begin=this.end=null;
			}
			else
			{
				begin=begin.next;
			}
			
		}

	}