package utility;

/**
 * The Class EmptyQueueException modella l'eccezzione che occorre qualora si cerca di
 * leggere/cancellare da una coda vuota
 * @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone  
 */
public class EmptyQueueException extends Throwable {
	
	/**
	 * Instanzia un empty queue exception.
	 */
	public EmptyQueueException(){};
	
	/* (non-Javadoc)
	 * @see java.lang.Throwable#getMessage()
	 */
	public String getMessage()
	{
		return "Empty Queue";
	}

}
