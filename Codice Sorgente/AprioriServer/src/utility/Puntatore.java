package utility;


/**
 * la classe Puntatore che modella il puntatore all'elemento successivo nella struttura dati lista linkata
 * @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone  
 */
public class Puntatore  {
	
		/** The link. */
		public Cella link;
		
		/**
		 * Instantiates un nuovo puntatore.
		 *
		 * @param c la cella
		 */
		public Puntatore(Cella c) {
			link = c;
		}
	}