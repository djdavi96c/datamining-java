package utility;


/**
 *  la classe Cella che modella un elemento singolo della struttura dati lista linkata 
 */
class Cella {
		
		/** The elemento. */
		Object elemento;
		
		/** The successivo. */
		Puntatore successivo=null; 

		/**
		 * Instantiates a new cella.
		 *
		 * @param e the e
		 */
		public Cella(Object e){
			elemento = e;
		}

	}
		