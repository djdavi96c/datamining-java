package utility;


/**
 * La classe LinkList  che modella una struttura dati lista linkatada usare come contenitore per i pattern frequenti e le regole confidenti.
 * @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone  
 */
public class LinkList {
	

	

	/** The inizio lista. */
	private Puntatore inizioLista = null;

		
		
	/**
	 * Checks if is empty. Cntrolla se la lista � vuota
	 *
	 * @return true, se � vuota
	 */
	public boolean isEmpty() {
			return inizioLista == null;
	}
		
	/**
	 * First list. 
	 *
	 * @return the puntatore
	 */
	public Puntatore firstList() {
			return null;	
		}	

	/**
	 * End list.
	 *
	 * @param p the p
	 * @return true, if successful
	 */
	public boolean endList(Puntatore p) {
			if (isEmpty()) return true;
			if (p == firstList())
				return inizioLista == null; // verifica che la lista sia vuota
			else
				return ((Puntatore)p).link.successivo == null; //verifica che elemento successivo a quello in posizione p sia nullo
		}


		/**
		 * Read list.
		 *
		 * @param p the p
		 * @return the object
		 */
		public Object readList(Puntatore p) {
			if (isEmpty())
				throw new IndexOutOfBoundsException("Lista vuota");
			if (p == firstList())
				return inizioLista.link.elemento;
			else
				return ((Puntatore) p).link.successivo.link.elemento;
			
				

		}
		
		/**
		 * Adds the.
		 *
		 * @param e the e
		 */
		public void add(Object e) { //aggiunge in testa
			Puntatore temp;

			if (!isEmpty()) {
					temp = inizioLista;
					inizioLista = new Puntatore(new Cella(e));
					inizioLista.link.successivo = temp;
				}
			 else {
				// se la lista � vuota
				inizioLista = new Puntatore(new Cella(e));
			}

		}

		
		/**
		 * Succ.
		 *
		 * @param p the p
		 * @return the puntatore
		 */
		public Puntatore succ(Puntatore p) {
			if (endList(p))
				throw new IndexOutOfBoundsException(
						"Posizione fine lista non valida");
			if (isEmpty())
				throw new IndexOutOfBoundsException("Lista vuota");
			if (p == firstList())
				return inizioLista;			
			else if (p == inizioLista)
				return inizioLista.link.successivo;
			else
				return ((Puntatore) p).link.successivo;
		}

	
}
