package database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;


import database.TableSchema.Column;



/**
 * La classe Table_Data (parzialmente fornita dal docente) che modella ll�insieme di tuple collezionate in una tabella. La singola tupla � modellata dalla classe Tuple_Data  inner class di Table_Data.
 *  @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone
 */
public class TableData
{
	
	/** Il db. */
	private DbAccess db;
	
	/**
	 * Db access.
	 *
	 * @param db il database
	 */
	public void DbAccess(DbAccess db)
	{
		this.db=db;
	}
	
	/**
	 * La classe TupleData.
	 */
	public class TupleData
	{
		
		/** Lista di tuple. */
		public List<Object> tuple=new ArrayList<Object>();
		
		/**
		 *	@return Restituisce i valori contenuti nella lista di tuple
		 */
		public String toString()
		{
			String value="";
			Iterator<Object> it= tuple.iterator();
			while(it.hasNext())
				value+= (it.next().toString() +" ");
			
			return value;
		}
	}
	
	/**
	 * Gets the transazioni.
	 * Ricava lo schema della tabella con nome table. Esegue una interrogazione per estrarre le tuple da tale tabella. Per ogni tupla del resultset, si crea un oggetto, istanza della classe Tupla, il cui riferimento va incluso nella lista da restituire. In particolare, per la tupla corrente nel resultset, si estraggono i valori dei singoli campi (usando getFloat() o getString()), e li si aggiungono all�oggetto istanza della classe Tupla che si sta costruendo.
	 * @param table nome della tabella nel database.
	 * @return the transazioni
	 * @throws SQLException the SQL exception
	 */
	public List<TupleData> getTransazioni(String table) throws SQLException
	{
		LinkedList<TupleData> transSet = new LinkedList<TupleData>();
		Statement statement;
		TableSchema tSchema=new TableSchema(db,table);
		String query="select ";
		
		for(int i=0;i<tSchema.getNumberOfAttributes();i++)
		{
			Column c=tSchema.getColumn(i);
			if(i>0)
				query+=",";
				query += c.getColumnName();
		}
		if(tSchema.getNumberOfAttributes()==0)
			throw new SQLException();
		query += (" FROM "+table);
		
		statement = db.getConnection().createStatement();
		ResultSet rs = statement.executeQuery(query);
		while (rs.next())
		{
			TupleData currentTuple=new TupleData();
			for(int i=0;i<tSchema.getNumberOfAttributes();i++)
				if(tSchema.getColumn(i).isNumber())
					currentTuple.tuple.add(rs.getFloat(i+1));
				else
					currentTuple.tuple.add(rs.getString(i+1));
			transSet.add(currentTuple);
		}
		rs.close();
		statement.close();

		return transSet;
	}
	
	/**
	 * Gets the distinct column vaues.
	 * Formula ed esegue una interrogazione SQL per estrarre i valori distinti ordinati di column e popolare una lista da restituire
	 * @param table Nome della tabella
	 * @param column nome della colonna nella tabella 
	 * @return  Lista di valori distinti ordinati in modalit� ascendente che l�attributo identificato da nome column assume nella tabella identificata dal nome table
	 * @throws SQLException the SQL exception
	 */
	public List<Object> getDistinctColumnVaues(String table, Column column) throws SQLException
	{
		List<Object> temp = new LinkedList<Object>();
		Statement statement;
		Object value;
		
		statement = db.getConnection().createStatement();
		String query = "SELECT DISTINCT " + column.getColumnName() + " FROM " + table + " ORDER BY " + column.getColumnName() + " ASC";
		ResultSet rs = statement.executeQuery(query);
		while(rs.next())
		{
			value = rs.getString(column.getColumnName());
			temp.add(value);
		}
		
		return temp;
	}

	
	/**
	 * Gets the aggregate column value.
	 * 
 Formula ed esegue una interrogazione SQL per estrarre il valore aggregato (valore minimo o valore massimo) cercato nella colonna di nome column della tabella di nome table.  Il metodo solleva e propaga una NoValueException se il resultset � vuoto o il valore calcolato � pari a null  
N.B.  aggregate � di tipo QUERY_TYPE dove QUERY_TYPE � la classe enumerativa
	 * @param table nome della tabella
	 * @param column nome della colonna
	 * @param aggregate the aggregate
	 * @return the aggregate column value
	 * @throws SQLException the SQL exception
	 * @throws NoValueException the no value exception
	 */
	public  Object getAggregateColumnValue(String table,Column column,QUERY_TYPE aggregate) throws SQLException,NoValueException{
		Statement statement;
		TableSchema tSchema=new TableSchema(db,table);
		Object value=null;
		String aggregateOp="";
		
		String query="select ";
		if(aggregate==QUERY_TYPE.MAX)
			aggregateOp+="max";
		else
			aggregateOp+="min";
		query+=aggregateOp+"("+column.getColumnName()+ ") FROM "+table;
		
		
		statement = db.getConnection().createStatement();
		ResultSet rs = statement.executeQuery(query);
		if (rs.next()) 
		{
				if(column.isNumber())
					value=rs.getFloat(1);
				else
					value=rs.getString(1);		
		}
		rs.close();
		statement.close();
		if(value==null) throw new NoValueException("No " + aggregateOp+ " on "+ column.getColumnName());
			
		return value;

	}
	
	

	

}
