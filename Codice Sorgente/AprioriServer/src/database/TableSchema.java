package database;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * La classe Table_Schema ( che modella lo schema di una
 * tabella nel database relazionale
 *  @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone
 */
public class TableSchema {

	/**
	 * Il Class Column.
	 */
	public class Column {

		/** Il name. */
		private String name;

		/** Il type. */
		private String type;

		/**
		 * Instantiates a new column.
		 *
		 * @param name
		 *            Il name
		 * @param type
		 *            Il type
		 */
		Column(String name, String type) {
			this.name = name;
			this.type = type;
		}

		/**
		 * Gets the column name. Ottiene il nome della colonna
		 *
		 * @return Il  nome della colonna
		 */
		public String getColumnName() {
			return name;
		}

		/**
		 * Checks if is number. Controlla se sia un numero
		 *
		 * @return true, se � un numero
		 */
		public boolean isNumber() {
			return type.equals("number");
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see java.lang.Object#toString()
		 */
		public String toString() {
			return name + ":" + type;
		}
	}

	/** Il table schema. */
	List<Column> tableSchema = new ArrayList<Column>();

	/**
	 * Instanzia un table schema
	 *
	 * @param db
	 *            Il db
	 * @param tableName
	 *            il nome della tabella
	 * @throws SQLException
	 *             the SQL exception
	 */
	public TableSchema(DbAccess db, String tableName) throws SQLException {
		HashMap<String, String> mapSQL_JAVATypes = new HashMap<String, String>();
		// http://java.sun.com/j2se/1.3/docs/guide/jdbc/getstart/mapping.html
		mapSQL_JAVATypes.put("CHAR", "string");
		mapSQL_JAVATypes.put("VARCHAR", "string");
		mapSQL_JAVATypes.put("LONGVARCHAR", "string");
		mapSQL_JAVATypes.put("BIT", "string");
		mapSQL_JAVATypes.put("SHORT", "number");
		mapSQL_JAVATypes.put("INT", "number");
		mapSQL_JAVATypes.put("LONG", "number");
		mapSQL_JAVATypes.put("FLOAT", "number");
		mapSQL_JAVATypes.put("DOUBLE", "number");

		Connection con = db.getConnection();
		DatabaseMetaData meta = con.getMetaData();
		ResultSet res = meta.getColumns(null, null, tableName, null);

		while (res.next()) {

			if (mapSQL_JAVATypes.containsKey(res.getString("TYPE_NAME")))
				tableSchema.add(
						new Column(res.getString("COLUMN_NAME"), mapSQL_JAVATypes.get(res.getString("TYPE_NAME"))));
		}
		res.close();
	}

	/**
	 * Gets Il number of attributes. Ottiene il numero di attributi
	 *
	 * @return Il numero di attributi
	 */
	public int getNumberOfAttributes() {
		return tableSchema.size();
	}

	/**
	 * Gets the column. Ottiene la colonna
	 *
	 * @param index
	 *           l' index
	 * @return l' column
	 */
	public Column getColumn(int index) {
		return tableSchema.get(index);
	}

}
