package database;


/**
 * The Enum QUERY_TYPE.
 *  @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone
 */
public enum QUERY_TYPE {

	/** Il min. */
	MIN,
	/** Il max. */
	MAX
}
