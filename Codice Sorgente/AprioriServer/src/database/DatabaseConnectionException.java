package database;


/**
 *  La classe DatabaseConnectionException che estende Exception per modellare il fallimento nella connessione al database.
 */
public class DatabaseConnectionException extends Exception 
{
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 908980918836060229L;
	
	/** Il messaggio */
	private String message;
	
	/**
	 * Istanzia un Database Connection Exception
	 *
	 * @param msg il messaggio
	 */
	DatabaseConnectionException(String msg)
	{
		this.message=msg;
	}
	
	/**
	 * getMessage ottiene il messaggio.
	 * @return Restituisce il messaggio contenuto nell'exception
	 */
	public String getMessage()
	{
		return this.message;
	}
}
