package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.management.InstanceNotFoundException;

/**
 * Gestisce l'accesso al DB per la lettura dei dati di training.
 *
 * @author Map Tutor
 */
public class DbAccess 
{
	
	/** The Constant DRIVER_CLASS_NAME. */
	private static final String DRIVER_CLASS_NAME = "org.gjt.mm.mysql.Driver";
	
	/** The Constant DBMS. */
	private static final String DBMS = "jdbc:mysql";
	
	/** Il server */
	private  final String SERVER = "localhost";
	
	/** La porta del server. */
	private  int PORT = 3306;
	
	/** Il nome del database */
	private  String DATABASE = "AprioriDB";
	
	/** L'username dell'utente per l'accesso al database*/
	private  String USER_ID = "AprioriUser";
	
	/** La password dell'utente */
	private  String PASSWORD = "apriori";

	/** Connessione */
	private Connection conn;
	
	/**
	 * Inizializza una connessione al DB.
	 * impartisce al class loader l�ordine di caricare il driver mysql, inizializza la connessione riferita da conn. Il metodo solleva e propaga una eccezione di tipo DatabaseConnectionException in caso di fallimento nella connessione al database
	 * @throws DatabaseConnectionException the database connection exception
	 */
	public  void initConnection() throws DatabaseConnectionException
	{
		String connectionString = DBMS+"://" + SERVER + ":" + PORT + "/" + DATABASE;
		try {
			
				Class.forName(DRIVER_CLASS_NAME).newInstance();
			} 
		catch (IllegalAccessException e) {
				
				e.printStackTrace();
				throw new DatabaseConnectionException(e.toString());
			}
		catch (InstantiationException e) {
					
					e.printStackTrace();
					throw new DatabaseConnectionException(e.toString());
			} 
		catch (ClassNotFoundException e) {
			System.out.println("Impossibile trovare il Driver: " + DRIVER_CLASS_NAME);
			throw new DatabaseConnectionException(e.toString());
		}
		
		try {
			conn = DriverManager.getConnection(connectionString, USER_ID, PASSWORD);
			
			} 
		catch (SQLException e) {
			System.out.println("Impossibile connettersi al DB");
			e.printStackTrace();
			throw new DatabaseConnectionException(e.toString());
		}
		
	}
	
	/**
	 * Gets the connection. Ottiene la connessione
	 *
	 * @return la variabile connection
	 */
	public Connection getConnection()
	{
		return conn;
	}
	
	/**
	 * Chiude la connessione
	 */
	public void closeConnection()
	{
		try {
			conn.close();
			}
		catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
