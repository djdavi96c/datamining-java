package database;

/**
 * La classe NoValueException che estende Exception per modellare l�assenza di
 * un valore all�interno di un resultset
 */
public class NoValueException extends Exception {

	/** Il messaggio. */
	private String message;

	/**
	 * Instanzia un No Value Exception
	 *
	 * @param msg
	 *            il messaggio
	 */
	NoValueException(String msg) {
		this.message = msg;
	}

	/**
	 * Restituisce il messaggio
	 * @return il messaggio
	 */
	public String getMessage() {
		return this.message;
	}
}
