package com.example.davide.aprioriclient;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.Formatter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class AprioriClient extends AppCompatActivity {
    String temp="Nothing to show";
    String nomeTabella="";
    float supp=0.0f;
    float conf=0.0f;

    //indirizzo del server
    public String address="192.168.1.103";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apriori_client);
        Button apriori=(Button)findViewById(R.id.Apriori);
        final RadioButton learning=(RadioButton)findViewById(R.id.learning);
        final RadioButton reading=(RadioButton)findViewById(R.id.reading);
        final EditText Data = (EditText) findViewById(R.id.data);
        final EditText minSup = (EditText) findViewById(R.id.minsup);
        final EditText minConf = (EditText) findViewById(R.id.minconf);
        //azione se il bottone è cliccato
        apriori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(AprioriClient.this, Result.class);
                if(learning.isChecked() && isNetworkAvailable()) {
                    if(Data.getText().toString().length()<1 || minSup.getText().toString().length()<1 || minConf.getText().toString().length()<1){
                        Toast.makeText(getBaseContext(),"Campi vuoti !",Toast.LENGTH_SHORT).show();
                        return;

                    }
                    nomeTabella = Data.getText().toString();
                    supp = Float.parseFloat(minSup.getText().toString());
                    conf = Float.parseFloat(minConf.getText().toString());
                    System.out.println("connesso");
                    new NetTaskLearning().execute();
                    intent.putExtra("key",temp);
                    Toast.makeText(getBaseContext(),"Completato Correttamente!",Toast.LENGTH_SHORT).show();
                    startActivity(intent);
                }else if(reading.isChecked() && isNetworkAvailable()){
                        new NetTaskReading().execute();
                        intent.putExtra("key",temp);
                        Toast.makeText(getBaseContext(),"Completato Correttamente!",Toast.LENGTH_SHORT).show();
                        startActivity(intent);
                }else{
                    Toast.makeText(getBaseContext(),"Connessione Assente!",Toast.LENGTH_SHORT).show();
                }



            }
        });
    }
  public class NetTaskLearning extends AsyncTask<String, Integer, String> implements Serializable
    {
        /**
         * NetLearning si occupa di imparare le regole inviando i dati al server che li
         * elabora e restituisce il pack di regole. Il server restituisce le regole
         * se esistono con la minConf e la minSup inserite. In caso negativo non restituisce nulla
         *
         * @param params
         * @return temp che contiene i pattern trovati in caso c'è ne siano altrimenti nulla.
         */
        @Override
        protected String doInBackground(String... params)
        {

            String temp="";
            try{
                //cabiare ip in base alla rete in cui ci si trova
                InetAddress addr=InetAddress.getByName(address);
                Socket socket=new Socket(addr,8080);
                ObjectOutputStream out=new ObjectOutputStream(socket.getOutputStream());
                ObjectInputStream in=new ObjectInputStream(socket.getInputStream());
                Integer caso=1;
                out.writeObject(caso);

                out.writeObject(nomeTabella);
                out.writeObject(supp);
                out.writeObject(conf);
                System.out.println(conf);
                temp=in.readObject().toString();
                System.out.println("temp"+temp);
                caso=2;
                out.writeObject(caso);
                socket.close();

            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return temp;
        }
        protected void onPostExecute(String result) {
            temp=result;
            System.out.println(result);
        }
    }
    public class NetTaskReading extends AsyncTask<String, Integer, String> implements Serializable
    {
        /**
         * NetTaskReading si occupa di leggere il file contenuto sul server che contiene
         * le regole
         * @param params
         * @return
         */
        @Override
        protected String doInBackground(String... params)
        {

            String temp="";
            try{
                //cabiare ip in base alla rete in cui ci si trova
                InetAddress addr=InetAddress.getByName(address);
                Socket socket = new Socket(addr, 8080);
                ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
                ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
                Integer caso = 3;
                out.writeObject(caso);
                temp = in.readObject().toString();
                socket.close();

            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return temp;
        }
        protected void onPostExecute(String result) {
            temp=result;
            System.out.println(result);
        }
    }


    /**
     * Controlla la presenenza di una connessione internet
     * @return true se la connessione è presente. false se la connessione non è presente
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        //WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        //String address = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());
        System.out.println(address);
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}

