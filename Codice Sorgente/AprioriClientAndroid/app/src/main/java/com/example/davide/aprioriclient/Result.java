package com.example.davide.aprioriclient;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.widget.EditText;
import android.widget.TextView;

public class Result extends AppCompatActivity {
    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        TextView print=(TextView) findViewById(R.id.res);
        String result=getIntent().getExtras().getString("key");
        print.setMovementMethod(new ScrollingMovementMethod());
        print.setText(result);



    }
}
