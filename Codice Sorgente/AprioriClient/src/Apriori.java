import java.awt.Dimension;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;


// TODO: Auto-generated Javadoc
/**
 * La Classe Apriori Apriori.
 *
 * @author Davide Caldarulo,Davide Ciccarone,Alessio Gernone
 */
public class Apriori extends javax.swing.JFrame {

    /** The Constant serialVersionUID. */
	private static final long serialVersionUID = 5548561060890265660L;
	
	/**
	 * Creates new form Apriori.
	 */
    public Apriori() {
        initComponents();
    }

    /**
     * Inits the components.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">                          
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        cpParameterSetting = new javax.swing.JPanel();
        cpAprioriMining = new javax.swing.JPanel();
        jRadioButton2 = new javax.swing.JRadioButton();
        buttonGroup1.add(jRadioButton2);
        jRadioButton3 = new javax.swing.JRadioButton();
        buttonGroup1.add(jRadioButton3);
        cpAprioriInput = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        cpRuleViewer = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        setMinimumSize(new Dimension(800, 300));
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        
        cpParameterSetting.setBorder(javax.swing.BorderFactory.createTitledBorder("Apriori"));

        cpAprioriMining.setBorder(javax.swing.BorderFactory.createTitledBorder("Selecting Data Source"));

        jRadioButton2.setText("Learning Rules from db");
        

        jRadioButton3.setText("Reading rules from file");

        javax.swing.GroupLayout gl_cpAprioriMining = new javax.swing.GroupLayout(cpAprioriMining);
        cpAprioriMining.setLayout(gl_cpAprioriMining);
        gl_cpAprioriMining.setHorizontalGroup(
            gl_cpAprioriMining.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(gl_cpAprioriMining.createSequentialGroup()
                .addContainerGap()
                .addGroup(gl_cpAprioriMining.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jRadioButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jRadioButton3))
                .addGap(56, 56, 56))
        );
        gl_cpAprioriMining.setVerticalGroup(
            gl_cpAprioriMining.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(gl_cpAprioriMining.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(jRadioButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jRadioButton3)
                .addGap(50, 50, 50))
        );

        cpAprioriInput.setBorder(javax.swing.BorderFactory.createTitledBorder("Input Parameters"));

        jLabel1.setText("Data");

        jLabel2.setText("min sup");

        jLabel3.setText("min conf");

        javax.swing.GroupLayout gl_cpAprioriInput = new javax.swing.GroupLayout(cpAprioriInput);
        cpAprioriInput.setLayout(gl_cpAprioriInput);
        gl_cpAprioriInput.setHorizontalGroup(
            gl_cpAprioriInput.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(gl_cpAprioriInput.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        gl_cpAprioriInput.setVerticalGroup(
            gl_cpAprioriInput.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(gl_cpAprioriInput.createSequentialGroup()
                .addGap(13, 13, 13)
                .addGroup(gl_cpAprioriInput.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextField2)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(87, 87, 87))
        );

        javax.swing.GroupLayout gl_cpParameterSetting = new javax.swing.GroupLayout(cpParameterSetting);
        cpParameterSetting.setLayout(gl_cpParameterSetting);
        gl_cpParameterSetting.setHorizontalGroup(
            gl_cpParameterSetting.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(gl_cpParameterSetting.createSequentialGroup()
                .addContainerGap()
                .addComponent(cpAprioriMining, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cpAprioriInput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        gl_cpParameterSetting.setVerticalGroup(
            gl_cpParameterSetting.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, gl_cpParameterSetting.createSequentialGroup()
                .addContainerGap()
                .addGroup(gl_cpParameterSetting.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cpAprioriMining, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cpAprioriInput, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        jButton1.setText("apriori");
        jButton1.setActionCommand("");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        cpRuleViewer.setBorder(javax.swing.BorderFactory.createTitledBorder("Patterns and Rules"));
        cpRuleViewer.setToolTipText("");

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        javax.swing.GroupLayout gl_cpRuleViewer = new javax.swing.GroupLayout(cpRuleViewer);
        cpRuleViewer.setLayout(gl_cpRuleViewer);
        gl_cpRuleViewer.setHorizontalGroup(
            gl_cpRuleViewer.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(gl_cpRuleViewer.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        gl_cpRuleViewer.setVerticalGroup(
            gl_cpRuleViewer.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, gl_cpRuleViewer.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(331, 331, 331)
                .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(275, 275, 275))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(cpRuleViewer, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cpParameterSetting, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(cpParameterSetting, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cpRuleViewer, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>                        

                                           

    /**
     * J button 1 action performed.
     *
     * @param evt the evt
     */
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {                                         
        // TODO add your handling code here:
    	String temp ="";
		if(jRadioButton2.isSelected())
		{
			String nomeTabella = jTextField1.getText();
			float minSupp = Float.parseFloat(jTextField2.getText());
			float minConf = Float.parseFloat(jTextField3.getText());
			try
			{
				InetAddress addr= InetAddress.getByName(null);
				Socket socket = new Socket(addr, 8080);
				ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
				ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
				
				Integer caso = 1;
				out.writeObject(caso);
				out.writeObject(nomeTabella);
				out.writeObject(minSupp);
				out.writeObject(minConf);
				temp =in.readObject().toString();
				jTextArea1.setText(temp);
				caso = 2;
				out.writeObject(caso);
				socket.close();
				
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		if(jRadioButton3.isSelected())
		{
			InetAddress addr;
			try
			{
				addr = InetAddress.getByName(null);
				Socket socket = new Socket(addr, 8080);
				ObjectOutputStream out = new ObjectOutputStream(socket.getOutputStream());
				ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
				Integer caso = 3;
				out.writeObject(caso);
				
				temp = in.readObject().toString();
				jTextArea1.setText(temp);
				socket.close();
			} catch (UnknownHostException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
								
		}
		
    }                                        

    /**
     * The main method.
     *
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Apriori.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Apriori.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Apriori.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Apriori.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Apriori().setVisible(true);
            }
        });
    }

    /** The button group 1. */
    // Variables declaration - do not modify                     
    private javax.swing.ButtonGroup buttonGroup1;
    
    /** The j button 1. */
    private javax.swing.JButton jButton1;
    
    /** The j label 1. */
    private javax.swing.JLabel jLabel1;
    
    /** The j label 2. */
    private javax.swing.JLabel jLabel2;
    
    /** The j label 3. */
    private javax.swing.JLabel jLabel3;
    
    /** The j panel 1. */
    private javax.swing.JPanel jPanel1;
    
    /** The cp parameter setting. */
    private javax.swing.JPanel cpParameterSetting;
    
    /** The cp apriori mining. */
    private javax.swing.JPanel cpAprioriMining;
    
    /** The cp apriori input. */
    private javax.swing.JPanel cpAprioriInput;
    
    /** The cp rule viewer. */
    private javax.swing.JPanel cpRuleViewer;
    
    /** The j radio button 2. */
    private javax.swing.JRadioButton jRadioButton2;
    
    /** The j radio button 3. */
    private javax.swing.JRadioButton jRadioButton3;
    
    /** The j scroll pane 1. */
    private javax.swing.JScrollPane jScrollPane1;
    
    /** The j text area 1. */
    private javax.swing.JTextArea jTextArea1;
    
    /** The j text field 1. */
    private javax.swing.JTextField jTextField1;
    
    /** The j text field 2. */
    private javax.swing.JTextField jTextField2;
    
    /** The j text field 3. */
    private javax.swing.JTextField jTextField3;
    // End of variables declaration                   
}
